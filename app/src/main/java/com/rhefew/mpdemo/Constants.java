package com.rhefew.mpdemo;

public class Constants {

    public static String PUBLIC_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88";
    public static String INTENT_EXTRA_PAYMENT_TYPE_ID = "payment_type_id";
    public static String INTENT_EXTRA_ISSUER_ID = "issuer_id";
    public static String INTENT_EXTRA_RECOMMENDED_MESSAGE = "recommended_message";
    public static float SELECTED_AMOUNT = 0f;

}

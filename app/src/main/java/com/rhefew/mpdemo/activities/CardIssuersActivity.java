package com.rhefew.mpdemo.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rhefew.mpdemo.Constants;
import com.rhefew.mpdemo.R;
import com.rhefew.mpdemo.adapters.GenericAdapter;
import com.rhefew.mpdemo.api.MercadoPagoAPI;
import com.rhefew.mpdemo.api.MercadoPagoService;
import com.rhefew.mpdemo.models.paymentmethods.CardIssuerModel;
import com.squareup.picasso.Picasso;

import java.time.format.TextStyle;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rhefew on 3/25/2018.
 */

public class CardIssuersActivity extends AppCompatActivity {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_payment_methods);
        getSupportActionBar().setTitle(R.string.actionbar_title_3);

        progressBar = findViewById(R.id.progressBar);
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(Constants.INTENT_EXTRA_PAYMENT_TYPE_ID)) {
            String paymentTypeId = extras.getString(Constants.INTENT_EXTRA_PAYMENT_TYPE_ID);

            //Calling card_issuers
            getCardIssuers(paymentTypeId);
        } else {
            Toast.makeText(this, "Error al obtener el método de pago seleccionado",
                    Toast.LENGTH_LONG).show();
        }
    }
    private void getCardIssuers(final String paymentTypeId) {
        //Retrieving MercadoPago API Interface
        progressBar.setVisibility(View.VISIBLE);
        MercadoPagoAPI api = MercadoPagoService.getInstance().getMercadoPagoAPI();
        api.getCardIssuers(Constants.PUBLIC_KEY, paymentTypeId).enqueue(new Callback<List<CardIssuerModel>>() {
            @Override
            public void onResponse(Call<List<CardIssuerModel>> call, Response<List<CardIssuerModel>> response) {
                if(response.isSuccessful()){
                    List<CardIssuerModel> model = response.body();
                    if(model.size() > 0) {
                        //Populating views with PaymentMethods response
                        createCardIssuersViews(model, paymentTypeId);
                    }else{
                        findViewById(R.id.errorLayout).setVisibility(View.VISIBLE);
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<CardIssuerModel>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(CardIssuersActivity.this, R.string.card_issuers_error,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void createCardIssuersViews(final List<CardIssuerModel> cardIssuers, final String paymentTypeId) {

        mRecyclerView = findViewById(R.id.paymentMethodsRecyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new GenericAdapter<CardIssuerModel>(cardIssuers) {
            @Override
            public void onBindViewHolder(GenericAdapter.ViewHolder holder, final int position) {

                holder.itemName.setText(cardIssuers.get(position).getName());
                Picasso.get().load(cardIssuers.get(position).getSecureThumbnail())
                        .into(holder.itemIcon);

                holder.rootView.setBackgroundColor(position %2==1? Color.parseColor("#FFFFFF")
                        : Color.parseColor("#1215799e"));

                holder.rootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(CardIssuersActivity.this, InstallmentsActivity.class);
                        intent.putExtra("payment_type_id", paymentTypeId);
                        intent.putExtra("issuer_id", cardIssuers.get(position).getId());
                        startActivity(intent);
                    }
                });
            }
        };
        mRecyclerView.setAdapter(mAdapter);
    }
}

package com.rhefew.mpdemo.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.rhefew.mpdemo.Constants;
import com.rhefew.mpdemo.R;
import com.rhefew.mpdemo.adapters.GenericAdapter;
import com.rhefew.mpdemo.api.MercadoPagoAPI;
import com.rhefew.mpdemo.api.MercadoPagoService;
import com.rhefew.mpdemo.models.paymentmethods.installments.InstallmentModel;
import com.rhefew.mpdemo.models.paymentmethods.installments.PayerCost;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rhefew on 3/25/2018.
 */

public class InstallmentsActivity extends AppCompatActivity {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_payment_methods);
        getSupportActionBar().setTitle(R.string.actionbar_title_4);

        progressBar = findViewById(R.id.progressBar);
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(Constants.INTENT_EXTRA_ISSUER_ID)) {
            String paymentTypeId = extras.getString(Constants.INTENT_EXTRA_PAYMENT_TYPE_ID);
            String issuerId = extras.getString(Constants.INTENT_EXTRA_ISSUER_ID);
            float price = Constants.SELECTED_AMOUNT;

            //Calling installments
            getInstallments(paymentTypeId, issuerId, price);
        } else {
            Toast.makeText(this, "Error al obtener las cuotas disponibles para este banco",
                    Toast.LENGTH_LONG).show();
        }
    }
    private void getInstallments(String paymentTypeId, String issuerId, float price) {
        //Retrieving MercadoPago API Interface
        progressBar.setVisibility(View.VISIBLE);
        MercadoPagoAPI api = MercadoPagoService.getInstance().getMercadoPagoAPI();
        api.getInstallments(Constants.PUBLIC_KEY, paymentTypeId, issuerId, price).enqueue(new Callback<List<InstallmentModel>>() {
            @Override
            public void onResponse(Call<List<InstallmentModel>> call, Response<List<InstallmentModel>> response) {
                if(response.isSuccessful()){
                    List<InstallmentModel> model = response.body();
                    //Populating views with PaymentMethods response
                    if (model != null && model.size() > 0) {
                        createInstallmentsViews(model.get(0).getPayerCosts());
                    }else{
                        findViewById(R.id.errorLayout).setVisibility(View.VISIBLE);
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<InstallmentModel>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(InstallmentsActivity.this, R.string.card_issuers_error,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void createInstallmentsViews(final List<PayerCost> installments) {

        mRecyclerView = findViewById(R.id.paymentMethodsRecyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new GenericAdapter<PayerCost>(installments) {
            @Override
            public void onBindViewHolder(GenericAdapter.ViewHolder holder, final int position) {

                final String recommendedMessage = installments.get(position).getRecommendedMessage();
                holder.itemName.setText(recommendedMessage);

                holder.rootView.setBackgroundColor(position %2==1? Color.parseColor("#FFFFFF")
                        : Color.parseColor("#1215799e"));

                holder.rootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(InstallmentsActivity.this, PriceInputActivity.class);
                        intent.putExtra(Constants.INTENT_EXTRA_RECOMMENDED_MESSAGE, recommendedMessage);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        };
        mRecyclerView.setAdapter(mAdapter);

    }
}

package com.rhefew.mpdemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.rhefew.mpdemo.Constants;
import com.rhefew.mpdemo.R;

public class PriceInputActivity extends AppCompatActivity implements TextWatcher {

    EditText editPrice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_input);

        getSupportActionBar().setTitle(R.string.actionbar_title_1);

        editPrice = findViewById(R.id.price);
        editPrice.addTextChangedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Bundle extras = getIntent().getExtras();
        if(extras!=null && extras.containsKey(Constants.INTENT_EXTRA_RECOMMENDED_MESSAGE)){
            getSupportActionBar().setTitle(R.string.actionbar_title_finish);
            editPrice.setVisibility(View.GONE);
            findViewById(R.id.acceptPrice).setVisibility(View.GONE);
            TextView result = findViewById(R.id.result);
            result.setText(extras.getString(Constants.INTENT_EXTRA_RECOMMENDED_MESSAGE));
        }
    }

    public void nextScreen(View view){
        Intent intent = new Intent(PriceInputActivity.this,
                PaymentMethodsActivity.class);
        startActivity(intent);

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        TextView priceError = findViewById(R.id.priceError);
        priceError.setVisibility((s.length() > 0
                && Float.parseFloat(s.toString()) > 250000) ? View.VISIBLE : View.GONE);

        findViewById(R.id.acceptPrice).setEnabled(s.length() > 0
                && Float.parseFloat(s.toString()) < 250000);

    }

    @Override
    public void afterTextChanged(Editable s) {

        if(s.toString().length() >0){
            float price = Float.parseFloat(s.toString());
            if(price < 250000){
                Constants.SELECTED_AMOUNT = price;;
            }else{
            }

        }
    }
}

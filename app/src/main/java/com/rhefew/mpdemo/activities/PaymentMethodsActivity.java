package com.rhefew.mpdemo.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.rhefew.mpdemo.Constants;
import com.rhefew.mpdemo.R;
import com.rhefew.mpdemo.adapters.GenericAdapter;
import com.rhefew.mpdemo.api.MercadoPagoAPI;
import com.rhefew.mpdemo.api.MercadoPagoService;
import com.rhefew.mpdemo.models.paymentmethods.PaymentMethodsModel;
import com.squareup.picasso.Picasso;

import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentMethodsActivity extends AppCompatActivity{

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_payment_methods);
        getSupportActionBar().setTitle(R.string.actionbar_title_2);

        progressBar = findViewById(R.id.progressBar);
        //Calling payment_methods
        getPaymentMethods();
    }

    private void getPaymentMethods() {
        progressBar.setVisibility(View.VISIBLE);
        //Retrieving MercadoPago API Interface
        MercadoPagoAPI api = MercadoPagoService.getInstance().getMercadoPagoAPI();
        api.getPaymentMethods(Constants.PUBLIC_KEY).enqueue(new Callback<List<PaymentMethodsModel>>() {
            @Override
            public void onResponse(Call<List<PaymentMethodsModel>> call, Response<List<PaymentMethodsModel>> response) {
                if(response.isSuccessful()){
                    List<PaymentMethodsModel> model = response.body();
                    //Populating views with PaymentMethods response
                    createPaymentMethodsViews(model);
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<PaymentMethodsModel>> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(PaymentMethodsActivity.this, R.string.payment_methods_error,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void createPaymentMethodsViews(final List<PaymentMethodsModel> paymentMethods) {

        //only keeping credit cards

        Iterator<PaymentMethodsModel> iterator = paymentMethods.iterator();

        while (iterator.hasNext()) {
            PaymentMethodsModel paymentMethod = iterator.next();

            if (!paymentMethod.getPaymentTypeId().equals("credit_card"))
                iterator.remove();
        }

        mRecyclerView = findViewById(R.id.paymentMethodsRecyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new GenericAdapter<PaymentMethodsModel>(paymentMethods) {
            @Override
            public void onBindViewHolder(GenericAdapter.ViewHolder holder, final int position) {

                holder.itemName.setText(paymentMethods.get(position).getName());
                Picasso.get().load(paymentMethods.get(position).getSecureThumbnail())
                        .into(holder.itemIcon);

                holder.rootView.setBackgroundColor(position %2==1? Color.parseColor("#FFFFFF")
                        : Color.parseColor("#1215799e"));

                holder.rootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(PaymentMethodsActivity.this, CardIssuersActivity.class);
                        intent.putExtra("payment_type_id", paymentMethods.get(position).getId());
                        startActivity(intent);
                    }
                });
            }
        };
        mRecyclerView.setAdapter(mAdapter);

    }
}

package com.rhefew.mpdemo.api;

import com.rhefew.mpdemo.models.paymentmethods.CardIssuerModel;
import com.rhefew.mpdemo.models.paymentmethods.PaymentMethodsModel;
import com.rhefew.mpdemo.models.paymentmethods.installments.InstallmentModel;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface MercadoPagoAPI {

    String baseURL = "https://api.mercadopago.com/v1/";

    @GET("payment_methods")
    Call<List<PaymentMethodsModel>> getPaymentMethods(@Query("public_key") String publicKey);

    @GET("payment_methods/card_issuers")
    Call<List<CardIssuerModel>> getCardIssuers(@Query("public_key") String publicKey,
                                               @Query("payment_method_id") String paymentMethodId);

    @GET("payment_methods/installments")
    Call<List<InstallmentModel>> getInstallments(@Query("public_key") String publicKey,
                                                 @Query("payment_method_id") String paymentMethodId,
                                                 @Query("issuer.id") String issuer_id,
                                                 @Query("amount") Float amount);
}

package com.rhefew.mpdemo.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MercadoPagoService {

    private static MercadoPagoService mercadoPagoService;
    private MercadoPagoAPI mercadoPagoAPI;

    private MercadoPagoService(){

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MercadoPagoAPI.baseURL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mercadoPagoAPI = retrofit.create(MercadoPagoAPI.class);
    }

    public static MercadoPagoService getInstance(){
        if(mercadoPagoService == null){
            mercadoPagoService = new MercadoPagoService();
        }

        return mercadoPagoService;
    }

    public MercadoPagoAPI getMercadoPagoAPI() {
        return mercadoPagoAPI;
    }
}

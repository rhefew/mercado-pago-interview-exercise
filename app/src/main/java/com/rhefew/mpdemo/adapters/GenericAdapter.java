package com.rhefew.mpdemo.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rhefew.mpdemo.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public abstract class GenericAdapter<T> extends RecyclerView.Adapter<GenericAdapter.ViewHolder> {
    private List<T> items;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout rootView;
        public TextView itemName;
        public ImageView itemIcon;
        ViewHolder(LinearLayout rootView) {
            super(rootView);
            this.rootView = rootView;
            this.itemIcon = rootView.findViewById(R.id.itemIcon);
            this.itemName = rootView.findViewById(R.id.itemName);
        }
    }

    protected GenericAdapter(List<T> items) {
        this.items = items;
    }

    @Override
    public GenericAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout mainView = (LinearLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_methods_item, parent, false);

        mainView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        return new ViewHolder(mainView);
    }

    @Override
    public abstract void onBindViewHolder(ViewHolder holder, int position);

    @Override
    public int getItemCount() {
        return items.size();
    }
}
